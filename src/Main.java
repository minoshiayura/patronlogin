import java.io.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*
    Aplicación que pide un nombre de usuario que tiene que respetar un patrón y el nombre de un archivo, si existe,
    muestra el contenido por consola.
 */
public class Main {
    public static void main(String[] args) {
        String usuario;
        Pattern patron;
        Matcher matcher;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Logger log = Logger.getLogger("Log.log");
        try {
            FileHandler fh = new FileHandler("Log.log", true);
            log.addHandler(fh);
            log.setUseParentHandlers(false);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            log.setLevel(Level.ALL);
            log.log(Level.WARNING, "Log.log");
            try{
                System.out.println("Introduce el usuario de hasta 8 carácteres: ");
                usuario = reader.readLine().toLowerCase();
                patron = Pattern.compile("[a-z]{1,8}");
                matcher = patron.matcher(usuario);
                if (matcher.find()){
                    System.out.println("Usuario valido.");
                    System.out.println("Introduzca el nombre del archivo\nMáximo 8 carácteres y una extensión de 3: ");
                    File archivo;
                    String nombreArchivo = reader.readLine();
                    patron = Pattern.compile("[a-zA-Z]{1,8}[.][a-z]{3}");
                    matcher = patron.matcher(nombreArchivo);
                    archivo = new File(nombreArchivo);
                    if(matcher.find()){
                        if(archivo.exists()){
                            System.out.println("El fichero existe");
                            int i;
                            FileReader fr = new FileReader(archivo);
                            char[] b = new char[50];
                            while((i = fr.read(b)) != -1){
                                System.out.println(b);
                            }
                            fr.close();
                        }else {
                            System.out.println("El archivo no existe.");
                        }
                    }else{
                        System.out.println("Formato de nombre incorrecto.");
                    }
                }else{
                    System.out.println("Nombre de usuario no valido.");
                }
            }catch (Exception e){
                System.err.println(e.getMessage());
            }
        }catch(SecurityException | IOException se){
            System.err.println(se.getMessage());
        }
    }
}